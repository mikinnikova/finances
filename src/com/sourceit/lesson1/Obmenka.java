package com.sourceit.lesson1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public  class Obmenka extends FinanceGroup implements Exchange {

    Map<String, Double> currencies;

    public Obmenka(String name, String address,double usd, double eur) {
       super(name, address);
        currencies = new HashMap<>();
        currencies.put("USD", usd);
        currencies.put("EUR", eur);

    }

    @Override
    public double convertUanToUsd(int uan) {
        return uan / currencies.get("USD");
    }

    @Override
    public double convertUanToEur(int uan) {
        return uan /currencies.get("EUR");
    }

    @Override
    public double convertUanToRub(int uan) throws IOException {

        return 0 ;
    }




    @Override
    public String getInfoObmen() {
        return super.getInfoObmen();
    }
}

