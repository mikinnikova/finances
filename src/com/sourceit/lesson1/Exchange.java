package com.sourceit.lesson1;

import java.io.IOException;


public interface Exchange {
    double convertUanToUsd (int uan) throws IOException;
    double convertUanToEur (int uan) throws IOException;
    double convertUanToRub (int uan) throws IOException;


}
