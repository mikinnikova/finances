package com.sourceit.lesson1;

import java.io.IOException;


public interface GiveKredits {
    double getKredit(int uan) throws IOException;
}
