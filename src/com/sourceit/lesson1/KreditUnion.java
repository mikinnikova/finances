package com.sourceit.lesson1;

import java.io.IOException;

public class KreditUnion extends FinanceGroup implements GiveKredits {

    double procent;
    public KreditUnion(String name, String address, double procent) {
        super(name, address);
        this.procent = procent;
    }

    @Override
    public double getKredit(int uan) throws IOException {
        if(uan>100_000){
            throw new IOException("Must be less then 100000 uan");
        }
        return uan +(uan*procent);
    }
    @Override
    public String getInfoKredit(){
        return super.getInfoKredit() + " " +procent;
    }
}
