package com.sourceit.lesson1;


public class FinanceGroup implements Comparable<FinanceGroup>{
    String name;
    String address;

    public FinanceGroup(String name, String address) {
        this.name = name;
        this.address = address;
    }

    @Override
    public String toString() {
        return "FinanceGroup{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public int compareTo(FinanceGroup o) {
        return name.compareTo(o.name);
    }



    public  String getInfoObmen(){
        return name +" " + address + " ";
    }
    public  String getInfoKredit(){
        return name +" " + address + " ";
    }
    public  String getInfoDepozit(){
        return name +" " + address + " ";
    }
    public  String getInfoSend(){
        return name +" " + address + " ";
    }






}
