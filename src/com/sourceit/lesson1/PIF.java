package com.sourceit.lesson1;


public class PIF extends FinanceGroup implements GetMoney {
double procent;


    public PIF(String name, String address, double procent) {
        super(name, address);
        this.procent = procent;
    }

    @Override
    public double getAmount(int uan, int forHowMach) {
        return  uan + ((uan*procent)* forHowMach);
    }

    @Override
    public  String getInfoDepozit(){
        return super.getInfoDepozit() + " " + procent+ " ";
    }
}
