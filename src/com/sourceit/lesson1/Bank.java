package com.sourceit.lesson1;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class Bank extends FinanceGroup implements Exchange, GiveKredits, GetMoney, SendMoney {
    int licentionYear;
   Map <String, Double> currencies;
    double kreditProcent;
    double depozitProcent;
    double sendProcent;

    public Bank(String name, String address, int licentionYear,  double usd, double eur, double rub, double kreditProcent, double depozitProcent, double sendProcent) {
        super(name, address);
        this.licentionYear = licentionYear;
        currencies = new HashMap<>();
        currencies.put("USD", usd);
        currencies.put("EUR", eur);
        currencies.put("RUB", rub);
        this.kreditProcent = kreditProcent;
        this.depozitProcent =depozitProcent;
        this.sendProcent = sendProcent;

    }



    @Override
    public double convertUanToUsd(int uan) throws IOException {

            if (uan > 12_000) {
                throw new IOException("Must be less then 12000 uan");
            }
            return (uan - 15) / currencies.get("USD");
        }


    @Override
    public double convertUanToEur(int uan) throws IOException {
        if (uan > 12_000) {
            throw new IOException("Must be less then 12000 uan");
        }
        return (uan - 15) / currencies.get("EUR");
    }

    @Override
    public double convertUanToRub(int uan) throws IOException {
        if (uan > 12_000) {
            throw new IOException("Must be less then 12000 uan");
        }
        return  (uan - 15) / currencies.get("RUB");
    }

    /*@Override
    public double convertUsdToUan(int usd) {
        return (usd*usds[1])-15;
    }

    @Override
    public double convertEurToUan(int eur) {
        return (eur*eurs[1])-15;
    }

    @Override
    public double convertRubToUan(int rub) {
        return (rub*rubs[1])-15;
    }
*/


    @Override
    public double getKredit(int uan) throws IOException {
        if(uan>200_000){
            throw new IOException("Must be less then 200000 uan");
        }
        return uan +(uan*kreditProcent);
    }

    @Override
    public double getAmount(int uan, int forHowMach) {
        return uan + ((uan*depozitProcent)* forHowMach);
    }

    @Override
    public double send(int uan) {
        return uan + ((uan * sendProcent)+5);
    }

    @Override
    public String getInfoObmen() {
        return super.getInfoObmen();
    }
    @Override
    public String getInfoKredit(){
        return super.getInfoKredit() + " " +kreditProcent+ " ";
    }
    @Override
    public  String getInfoDepozit(){
        return super.getInfoDepozit() + " " + depozitProcent+ " ";
    }
    @Override
    public  String getInfoSend(){
        return super.getInfoSend() + sendProcent + " ";
    }
}
