package com.sourceit.lesson1;

import java.io.IOException;

public class Lombard extends FinanceGroup implements GiveKredits{

double procent;
int sum = 50_000;

    public Lombard(String name, String address, double procent) {
        super(name, address);
        this.procent = procent;
    }

    @Override
    public double getKredit(int uan)throws IOException {
        if(uan> sum){
            throw new IOException("Must be less then 50000 uan");
        }

        return uan +(uan*procent);
    }
    @Override
    public String getInfoKredit(){
        return super.getInfoKredit() + " " +procent;
    }
}
