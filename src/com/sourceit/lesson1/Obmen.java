package com.sourceit.lesson1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class Obmen  {
    private ArrayList<FinanceGroup> financeGroups;


    public Obmen() {
        financeGroups = generate();
    }

    private ArrayList generate() {
        ArrayList<FinanceGroup> financeGroups = new ArrayList<>();

        financeGroups.add(new Obmenka("Pol", "st.Longra", 26, 29));
        financeGroups.add(new Obmenka("Roaz", "st.Langra", 26.8f, 27.9f));
        financeGroups.add(new Lombard("Opa", "st.Liht", 0.4));
        financeGroups.add(new KreditCafe("Uti", "st Yuouj", 2));
        financeGroups.add(new KreditUnion("Uydt", "st.Opala", 0.2));
        financeGroups.add(new Bank("PrivatBank", "st.Vasnetchova", 2016, 25.6, 26, 0.45, 0.25, 0.24, 0.01));
        financeGroups.add(new Bank("OshadBank", "st.Popova", 2014, 24.3, 26.6, 27.2f,0.44, 0.25,  0.01));
        financeGroups.add(new PIF("Trololo", "st.Post", 0.35));
        financeGroups.add(new Post("Nova", "st.L", 0.02));



        return financeGroups;
    }
public void sorter(){
    Collections.sort(financeGroups);
    for (FinanceGroup financeGroup : financeGroups) {
        System.out.println(financeGroup);

    }
}


    public void getBestUsd() throws IOException {
        FinanceGroup bestExchange = financeGroups.get(0);
        double maxUsd = 0.0;


        for (FinanceGroup financeGroup : financeGroups) {
            if (financeGroup instanceof Exchange) {
                try {

                    double currentUsd = ((Exchange) financeGroup).convertUanToUsd(20_000);

                    if (maxUsd < currentUsd) {
                        bestExchange = financeGroup;
                        maxUsd = currentUsd;

                    }
                } catch (IOException e) {
                    System.out.println(String.format("Sorry,bank %s can`t convert amount more than 12_000", financeGroup.name));
                }
            }
        }
        System.out.println("Your money converted to usd: " + bestExchange.getInfoObmen() + maxUsd);
    }


    public void getBestEur() throws IOException {
        FinanceGroup bestExchange = financeGroups.get(0);
        double maxEur = 0.0;


        for (FinanceGroup financeGroup : financeGroups) {
            if (financeGroup instanceof Exchange) {
                try {

                    double currentUsd = ((Exchange) financeGroup).convertUanToEur(20_000);

                    if (maxEur < currentUsd) {
                        bestExchange = financeGroup;
                        maxEur = currentUsd;

                    }
                } catch (IOException e) {
                    System.out.println(String.format("Sorry,bank %s can`t convert amount more than 12_000", financeGroup.name));
                }
            }
        }

        System.out.println("Your money converted to eur: " + bestExchange.getInfoObmen() + maxEur);

    }


    public void getBestRub() throws IOException {
        FinanceGroup bestExchange = financeGroups.get(0);
        double maxRub = 0.0;


        for (FinanceGroup financeGroup : financeGroups) {
            if (financeGroup instanceof Exchange) {
                try {


                    double currentRub = ((Exchange) financeGroup).convertUanToRub(10_000);

                    if (maxRub < currentRub) {
                        bestExchange = financeGroup;
                        maxRub = currentRub;

                    }
                } catch (IOException e) {
                    System.out.println(String.format("Sorry,bank %s can`t convert amount more than 12_000", financeGroup.name));
                }
            }
        }


        System.out.println("Your money converted to rub: " + bestExchange.getInfoObmen() + maxRub);
    }

    public void getMinProcentPoKredit() throws IOException {
        FinanceGroup bestKredit = financeGroups.get(2);
        double goodForYou = 0.0;

        for (FinanceGroup financeGroup : financeGroups) {
            try {
                if (financeGroup instanceof GiveKredits) {
                    double currentKredit = ((GiveKredits) financeGroup).getKredit(50_000);

                    if (goodForYou > currentKredit) {
                        bestKredit = financeGroup;
                        goodForYou = currentKredit;
                    }
                }
            } catch (IOException e) {
                System.out.println(String.format("Sorry,%s can`t give so big kredit", financeGroup.name));
            }
        }
        System.out.println("Your money you have to return to " + bestKredit.getInfoKredit() + goodForYou);
    }

    public void findBestDepozit() {

        FinanceGroup bestDepozit = financeGroups.get(0);

        double goodForYou = 0.0;

        for (FinanceGroup financeGroup : financeGroups) {
            if (financeGroup instanceof GetMoney) {

                double currentDepozit = ((GetMoney) financeGroup).getAmount(50_000, 1);

                if (goodForYou < currentDepozit) {
                    bestDepozit = financeGroup;
                    goodForYou = currentDepozit;
                }

            }
        }


        System.out.println("Your money you have to get from " + bestDepozit.getInfoDepozit() + goodForYou);
    }

    public void findSender() {
        FinanceGroup bestSend = financeGroups.get(0);
        double goodForYou = 0.0;

        for (FinanceGroup financeGroup : financeGroups) {
            if (financeGroup instanceof SendMoney) {
                double currentSend = ((SendMoney) financeGroup).send(1000);

                if (goodForYou < currentSend) {
                    bestSend = financeGroup;
                    goodForYou = currentSend;
                }

            }
        }
        System.out.println("You will pay " + bestSend.getInfoSend() + goodForYou);
    }





    }


