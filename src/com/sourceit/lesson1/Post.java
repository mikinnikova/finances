package com.sourceit.lesson1;


public class Post extends FinanceGroup implements SendMoney {
    double procent;

    public Post(String name, String address, double procent) {
        super(name, address);
        this.procent =procent;
    }

    @Override
    public double send(int uan) {
        return uan + (uan*procent);
    }
    @Override
    public  String getInfoSend(){
        return super.getInfoSend() + procent + " ";
    }
}
